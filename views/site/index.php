<?php

/* @var $this yii\web\View */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\models\Location;

if (!empty($places['results'])) {
    $name = $location->name;
    $lat = $location->lat;
    $long = $location->long;

    $this->registerJs("main.loadMap($lat, $long);");

    $this->registerJs("main.addMarker(
        main.createLocation($lat, $long),
        \"$name\",
        true
    );");
} else {
    $this->registerJs('main.loadMap();');
}

$this->title = 'Places Nearby';
?>
<div class="site-index">
    <div class="row">
        <div class="col-lg-8">
            <div id="map-canvas" style="background: #ccc;height: 500px;">
            </div>
        </div>
        <div class="col-lg-4">
            <h3>Locations</h3>
            <hr>
            <?php $form = ActiveForm::begin([
                'action' => '/',
                'method' => 'get',
            ]); ?>

            <div class="form-group">
                <?= Html::dropDownList('location_id', $location->id, Location::all(), [
                    'class' => 'form-control',
                    'prompt' => 'Select a Location',
                ]) ?>
            </div>

            <div class="clearfix">
                <?= Html::submitButton('search', ['class' => 'btn btn-primary pull-right']) ?>
            </div>

            <?php ActiveForm::end(); ?>

            <?php if (!empty($places['results'])): ?>
                <hr>

                <h3>What's Nearby?</h3>

                <ul>
                    <?php foreach ($places['results'] as $place): ?>
                        <li><?= $place['name'] ?></li>
                        <?php
                            $name = $place['name'];
                            $icon = $place['icon'];
                            $lat = $place['geometry']['location']['lat'];
                            $long = $place['geometry']['location']['lng'];

                            $this->registerJs("main.addMarker(
                                main.createLocation($lat, $long),
                                \"$name\",
                                \"$icon\"
                            );");
                        ?>
                    <?php endforeach ?>
                </ul>
            <?php endif ?>
        </div>
    </div>
</div>
