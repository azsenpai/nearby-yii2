<?php

use yii\db\Migration;

class m170312_124043_location extends Migration
{
    const TABLE_NAME = 'location';

    public function up()
    {
        $this->createTable(
            self::TABLE_NAME,
            [
                'id' => $this->primaryKey(),
                'name' => $this->string()->notNull(),
                'lat' => $this->double()->notNull(),
                'long' => $this->double()->notNull(),
                'city' => $this->string()->notNull(),
                'state' => $this->string()->notNull(),
                'created_at' => $this->integer(),
                'updated_at' => $this->integer(),
            ]
        );
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
