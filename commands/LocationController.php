<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\helpers\Json;
use app\models\Location;

/**
 *
 */
class LocationController extends Controller
{
    /**
     *
     */
    public function getData()
    {
        $filename = Yii::getAlias('@app/data/parks.json');
        $json = file_get_contents($filename);

        return Json::decode($json);
    }

    /**
     *
     */
    public function actionImport()
    {
        $this->stdout('Loading Data ...' . PHP_EOL);
        $data = $this->getData();

        $this->stdout('Truncating old data ...' . PHP_EOL);
        Location::deleteAll();

        $this->stdout('Importing Data ...' . PHP_EOL);

        foreach ($data as $id => $item) {
            $model = new Location();
            $model->attributes = $item;
            if ($model->save()) {
                $this->stdout(sprintf('%d is saved', $id) . PHP_EOL);
            } else {
                $this->stdout(print_r($model->errors, true) . PHP_EOL);
            }
        }

        $this->stdout('Done' . PHP_EOL);
    }
}
