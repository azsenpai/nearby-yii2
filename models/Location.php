<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "location".
 *
 * @property integer $id
 * @property string $name
 * @property double $lat
 * @property double $long
 * @property string $city
 * @property string $state
 * @property integer $created_at
 * @property integer $updated_at
 */
class Location extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'location';
    }

    /**
     *
     */
    public static function all()
    {
        $locations = self::find()
            ->select(['id', 'name'])
            ->asArray()
            ->all();

        return ArrayHelper::map($locations, 'id', 'name');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'lat', 'long', 'city', 'state'], 'required'],
            [['lat', 'long'], 'number'],
            [['created_at', 'updated_at'], 'integer'],
            [['name', 'city', 'state'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'lat' => 'Lat',
            'long' => 'Long',
            'city' => 'City',
            'state' => 'State',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
