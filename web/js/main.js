var main = {

    map: null,

    mapOptions: {},

    markers: [],

    loadMap: function(lat, lng) {
        var zoom = 16;

        if (lat == undefined && lng == undefined) {
            zoom = 13;
            lat = 41.878114;
            lng = -87.629798;
        }

        main.mapOptions = {
            zoom: zoom,
            center: main.createLocation(lat, lng),
        };

        main.map = new google.maps.Map(document.getElementById('map-canvas'), main.mapOptions);
    },

    createLocation: function(lat, lng) {
        return new google.maps.LatLng(lat,lng);
    },

    addMarker: function(position, title, icon) {
        if (icon === true) {
            var pinColor = '2F76EE';
            icon = new google.maps.MarkerImage(
                'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|' + pinColor,
                new google.maps.Size(21, 34),
                new google.maps.Point(0, 0),
                new google.maps.Point(10, 34)
            );
        } else {
            icon = new google.maps.MarkerImage(
                icon,
                new google.maps.Size(71, 71),
                new google.maps.Point(0, 0),
                new google.maps.Point(17, 34),
                new google.maps.Size(25, 25)
            );
        }

        var marker = new google.maps.Marker({
            position: position,
            title: title,
            icon: icon,
            types: [
                'clothing_store',
                'convenience_store',
                'book_store',
                'shoe_store',
                'store',
                'restaurant',
                'transit_station',
                'food'
            ]
        });

        main.markers.push(marker);

        marker.setMap(main.map);
    },

    clearMarkers: function() {
        $(main.markers).each(function() {
            this.setMap(null);
        });

        main.markers = [];
    }

};

